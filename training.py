from torch import nn
import torch

from model import CharDataset, SlmDecoder

block_size = 128
batch_size = 128
# nb_layer = 12
# embedding_dim = 768
# nb_head = 8
nb_layer = 6
nb_head = 6
embedding_dim = 192
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

data = open('input.txt', 'r').read()

dataset = CharDataset(block_size, data)
dataloader = torch.utils.data.dataloader.DataLoader(
	dataset,
	sampler=torch.utils.data.RandomSampler(dataset, replacement=True),
	shuffle=False,
	batch_size=batch_size
)

model = SlmDecoder(nb_layer, embedding_dim, nb_head, dataset.get_vocab_size(), block_size).to(device)

lr, nb_iter = 5e-4, 6000
optimizer = torch.optim.Adam(model.parameters(), lr = lr)
criterion = nn.CrossEntropyLoss(ignore_index=-1)

checkpoint_name = 'checkpoint.pth'
nb_iter_finished = 0
step_losses = []
try:
	checkpoint = torch.load(checkpoint_name)
	nb_iter_finished = checkpoint['nb_iter_finished']
	model.load_state_dict(checkpoint['model_state'])
	optimizer.load_state_dict(checkpoint['optimizer_state'])
	step_losses = checkpoint['losses']
	print(f'Checkpoint loaded with {nb_iter_finished} iter finished.')
except FileNotFoundError:
	print('Starting from scratch.')
except:
	print('Error when loading the checkpoint.')
	exit(1)

data_iter = iter(dataloader)

model.train()
for e in range(nb_iter_finished, nb_iter):

	try:
		batch = next(data_iter)
	except StopIteration:
		data_iter = iter(dataloader)
		batch = next(data_iter)
	
	train_input, train_target = batch
	train_input = train_input.to(device)
	train_target = train_target.to(device)

	output = model(train_input)

	loss = criterion(output.view(-1, output.size(-1)), train_target.view(-1))

	optimizer.zero_grad()
	loss.backward()
	optimizer.step()

	step_losses.append(loss.item())

	if e % 10 == 0:
		print(f'iter {e}: train loss {loss.item():.5f}')

	if (e + 1) % 250 == 0:
		checkpoint = {
			'nb_iter_finished': e + 1,
			'model_state': model.state_dict(),
			'optimizer_state': optimizer.state_dict(),
			'losses': step_losses
		}

		torch.save(checkpoint, checkpoint_name)