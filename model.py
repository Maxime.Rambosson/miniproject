from torch.utils.data import Dataset

import torch
from torch import nn
from torch.nn import functional as F
import math

class CharDataset(Dataset):
	"""
	Emits batches of characters.

	Adapted from "https://github.com/karpathy/minGPT".
	"""

	def __init__(self, block_size, data):

		chars = list(set(list(data))) # get characters from the input data
		chars.sort()
		self.stoi = { ch:i for i,ch in enumerate(chars) } # map characters to integer indices
		self.itos = { i:ch for i,ch in enumerate(chars) } # map integer indices to characters

		self.block_size = block_size

		self.data = list(data)

	def get_vocab_size(self):
		return len(self.stoi)

	def __len__(self):
		return len(self.data) - self.block_size

	def __getitem__(self, idx):
		# grab a chunk of (block_size + 1) characters from the data
		data_block = self.data[idx:idx + self.block_size + 1]

		# encode every character to an integer
		encoded_block = [self.stoi[c] for c in data_block]

		# return the chunk and the shifted version as tensors
		x = torch.Tensor(encoded_block[:-1]).to(torch.long)
		y = torch.Tensor(encoded_block[1:]).to(torch.long)

		return x, y
	
class CausalMultiheadAttention(nn.Module):
    def __init__(self, embedding_dim, n_head, block_size):
        super().__init__()

        self.Wqkv = nn.Linear(embedding_dim, 3 * embedding_dim)
        self.Wo = nn.Linear(embedding_dim, embedding_dim)

        self.attn_drop = nn.Dropout(0.1)
        self.out_drop = nn.Dropout(0.1)

        self.register_buffer("causal_mask", torch.tril(torch.ones(block_size, block_size))
                                     .view(1, 1, block_size, block_size))
		
        self.nh = n_head
        self.hs = embedding_dim // n_head

    def forward(self, x):
        B, N, E = x.size()

        q, k ,v = self.Wqkv(x).reshape(B, N, 3, self.nh, self.hs).transpose(1, 3).unbind(2)

		# (B, NH, S, S) = (B, NH, S, HS) @ (B, NH, HS, S)
        attn = q @ k.transpose(-2, -1)
        attn = attn / math.sqrt(self.hs)

        attn = attn.masked_fill(self.causal_mask[:,:,:N,:N] == 0, float('-inf'))
        attn = F.softmax(attn, dim=-1)
        attn = self.attn_drop(attn)
        y = attn @ v # (B, nh, T, hs) = (B, nh, T, T) @ (B, nh, T, hs)
        
        y = y.transpose(1, 2).contiguous().view(B, N, E)

        y = self.out_drop(self.Wo(y))

        return y

class BlockTransformer(nn.Module):
	def __init__(self, embedding_dim, nb_head, block_size):
		super().__init__()

		self.causal_self_attn = CausalMultiheadAttention(embedding_dim, nb_head, block_size)

		self.MLP = nn.Sequential(
			nn.Linear(embedding_dim, 2 * embedding_dim),
			nn.ReLU(),
			nn.Linear(2 * embedding_dim, embedding_dim)
		)

		self.layer_norm_1 = nn.LayerNorm(embedding_dim)
		self.layer_norm_2 = nn.LayerNorm(embedding_dim)


	def forward(self, x):
		x = x + self.causal_self_attn(self.layer_norm_1(x))
		out = x + self.MLP(self.layer_norm_2(x))

		return out

def WPE(pos: torch.Tensor, embedding_dim):
	assert embedding_dim % 2 == 0
	assert pos.dim() == 2
	embedded = torch.zeros(pos.size()[0], pos.size()[1], embedding_dim)

	embedding_idx = torch.arange(0, embedding_dim) # size() = embedding_dim
	power = torch.pow(10000, embedding_idx / embedding_dim) # size() = embedding_dim
	frac = pos.unsqueeze(2) / power.unsqueeze(0) # size() = (pos.size(), embedding_dim)

	embedded[:, 0::2] = torch.sin(frac[:, 0::2])
	embedded[:, 1::2] = torch.cos(frac[:, 1::2])

	return embedded

class SlmDecoder(nn.Module):
	def __init__(self, nb_layer, embedding_dim, nb_head, vocab_size, block_size):
		super().__init__()

		self.embedding_dim = embedding_dim
		self.block_size = block_size

		self.WTE = nn.Embedding(vocab_size, embedding_dim)
		self.dropout = nn.Dropout()

		self.blocks = nn.ModuleList()
		for _ in range(nb_layer):
			self.blocks.append(BlockTransformer(embedding_dim, nb_head, block_size))

		self.final_layer_norm = nn.LayerNorm(embedding_dim)
		self.lm_head = nn.Linear(embedding_dim, vocab_size)

	def forward(self, x):
		_, block_size = x.size()

		pos = torch.arange(0, block_size).unsqueeze(0)

		tok_emb = self.WTE(x) # token embeddings
		pos_emb = WPE(pos, self.embedding_dim).to(x.device) # position embeddings
		x = self.dropout(tok_emb + pos_emb)

		for block in self.blocks:
			x = block(x)

		x = self.final_layer_norm(x)
		logits = self.lm_head(x) # Langage Modeling Head

		return logits

	def generate(self, idx, nb_new_tokens):
		for _ in range(nb_new_tokens):
			idx_cond = idx if idx.size(1) <= self.block_size else idx[:, -self.block_size:]

			logits = self(idx_cond)
			logits = logits[:, -1, :]

			v, _ = torch.topk(logits, 10)
			logits[logits < v[:, [-1]]] = -float('Inf')
			probs = F.softmax(logits, dim=-1)

			idx_next = torch.multinomial(probs, num_samples=1)

			idx = torch.cat((idx, idx_next), dim=1)

		return idx