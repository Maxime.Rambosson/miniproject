from model import CharDataset, SlmDecoder
import torch

def tokenize(context, dataset: CharDataset):
  return torch.tensor([dataset.stoi[c] for c in context], dtype=torch.long).unsqueeze(0)

def tokens_to_string(tokens, dataset: CharDataset):
    return [dataset.itos[int(i)] for i in tokens]

block_size = 128
batch_size = 128
# nb_layer = 12
# embedding_dim = 768
# nb_head = 8
nb_layer = 6
nb_head = 6
embedding_dim = 192
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

data = open('input.txt', 'r').read()
dataset = CharDataset(block_size, data)

model = SlmDecoder(nb_layer, embedding_dim, nb_head, dataset.get_vocab_size(), block_size).to(device)

checkpoint = torch.load('models/small.pth', map_location=device)
nb_epochs_finished = checkpoint['nb_iter_finished']
model.load_state_dict(checkpoint['model_state'])

model.eval()
with torch.no_grad():
    context = "O God, O God!"
    tokenized_context = tokenize(context, dataset).to(device)
    # the model should implement a method to generate tokens given a prompt
    y = model.generate(tokenized_context, 500)[0]
    completion = tokens_to_string(y, dataset)
    print(''.join(completion))